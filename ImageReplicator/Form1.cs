﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageReplicator
{
    public partial class Form1 : Form
    {
        private bool draw = false;
        private int dots = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(pictureBoxOriginal.Image.Width, pictureBoxOriginal.Image.Height);
            Bitmap original = new Bitmap(pictureBoxOriginal.Image);

            image = original.PrewittFilter(PencilMode.Checked);

            pictureBoxReplica.Image = image;
        }

        private void pictureBoxReplica_Click(object sender, EventArgs e)
        {
            if(saveButton.Enabled)
            {
                saveButton_Click(sender, e);
            }
        }
        
        private void pictureBoxReplica_Paint(object sender, PaintEventArgs gr)
        {
            
        }
        
        private void loadButton_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            DialogResult result = openFileDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            pictureBoxOriginal.Image = Image.FromFile(openFileDialog.FileName);

            StartButton.Enabled = true;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "PNG Image|*.png|JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            DialogResult result = saveFileDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            Stream stream = saveFileDialog.OpenFile();

            saveFileDialog.AddExtension = true;
            
            switch (saveFileDialog.FilterIndex)
            {
                case 1:
                    pictureBoxReplica.Image.Save(stream, ImageFormat.Png);
                    break;

                case 2:
                    pictureBoxReplica.Image.Save(stream, ImageFormat.Jpeg);
                    break;

                case 3:
                    pictureBoxReplica.Image.Save(stream, ImageFormat.Bmp);
                    break;

                case 4:
                    pictureBoxReplica.Image.Save(stream, ImageFormat.Gif);
                    break;
            }

            stream.Close();
        }
        
        private void pictureBoxOriginal_Click(object sender, EventArgs e)
        {
            if(loadButton.Enabled)
            {
                loadButton_Click(sender, e);
            }
        }
    }
}

